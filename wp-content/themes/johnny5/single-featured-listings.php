<?php get_header(); ?>
			
			<div id="content">
				
				<div class="header-image">
					
					<?php if(!((get_post_meta($post->ID, 'wpcf-header-image', TRUE))=='')){ 
						echo types_render_field("header-image", array("size"=>"johnny5-header"));
					 } ?>
					
				</div>
				
				<div id="inner-content">
					
					<div class="intro-bar">
						<div class="row">
							<div class="large-12 columns">
							</div>
						</div>
					</div>
					
					<div class="row" id="main" role="main">
			
					    <div class="large-12 columns clearfix">

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						
								<header class="article-header">
							
									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
						
								</header> <!-- end article header -->
					
								<section class="entry-content clearfix" itemprop="articleBody">
									<?php the_content(); ?>
									<?php if(!((get_post_meta($post->ID, 'wpcf-list-price', TRUE))=='')){ ?>
										<p><strong>Listing Price:</strong> $<?php echo types_render_field("list-price", array("size"=>"johnny5-header"));?></p>
					 				<?php } ?>
					 				<?php if(!((get_post_meta($post->ID, 'wpcf-idx-listing-link', TRUE))=='')){ ?>
										<a href="<?php echo types_render_field("idx-listing-link", array("raw"=>"true"));?>" class="button">View Full Listing</a>
					 				<?php } ?>
								</section> <!-- end article section -->
						
								<footer class="article-footer">
									<?php the_tags('<p class="tags"><span class="tags-title">' . __('Tags:', 'johnny5theme') . '</span> ', ', ', '</p>'); ?>
								</footer> <!-- end article footer -->
					
								<?php //comments_template(); ?>
					
							</article> <!-- end article -->
					
						<?php endwhile; ?>		
					
						<?php else : ?>
					
							<article id="post-not-found" class="hentry clearfix">
					    		<header class="article-header">
					    			<h1><?php _e("Oops, Post Not Found!", "johnny5theme"); ?></h1>
					    		</header>
					    		<section class="entry-content">
					    			<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "johnny5theme"); ?></p>
					    		</section>
					    		<footer class="article-footer">
					    		    <p><?php _e("This is the error message in the single.php template.", "johnny5theme"); ?></p>
					    		</footer>
							</article>
					
						<?php endif; ?>
						
			
					</div> <!-- end #main -->

				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
