<?php get_header(); ?>
			
			<div id="content">
				
				<div class="header-image">
					<?php $page_header = of_get_option('page_header'); ?>
					
					<?php if (!empty($page_header)){ ?>
					 	
					 	<img src="<?php echo home_url(); ?>/<?php echo $page_header; ?>">

					<?php } ?>
				</div>
				
				<div id="inner-content">
					
					<div class="intro-bar">
						<div class="row">
							<div class="large-12 columns">
							</div>
						</div>
					</div>
					
					<div class="row" id="main" role="main">
			
					    <div class="large-12 columns clearfix">
				
					    <?php if (is_category()) { ?>
					    
						    <h1 class="page-title"><?php single_cat_title(); ?></h1>
					    
					    <?php } elseif (is_tag()) { ?> 
						    
						    <h1 class="page-title"><?php single_tag_title(); ?></h1>
					    
					    <?php } elseif (is_author()) { 
					    	global $post;
					    	$author_id = $post->post_author;
					    ?>
					    
						    <h1 class="page-title"><span><?php _e("Posts By:", "johnny5theme"); ?></span> <?php echo get_the_author_meta('display_name', $author_id); ?></h1>
					    
					    <?php } elseif (is_day()) { ?>
					    
						    <h1 class="page-title"><span><?php _e("Daily Archives:", "johnny5theme"); ?></span> <?php the_time('l, F j, Y'); ?></h1>
		
		    			<?php } elseif (is_month()) { ?>
			    		    
			    		    <h1 class="page-title"><span><?php _e("Monthly Archives:", "johnny5theme"); ?></span> <?php the_time('F Y'); ?></h1>
					
					    <?php } elseif (is_year()) { ?>
					        
					        <h1 class="page-title"><span><?php _e("Yearly Archives:", "johnny5theme"); ?></span> <?php the_time('Y'); ?></h1>
					        
					    <?php } ?>

					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						    <header class="article-header">
							
							    <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>		
						
						    </header> <!-- end article header -->
					
						    <section class="entry-content clearfix">
						
							    <?php the_post_thumbnail( 'johnny5-thumb-300' ); ?>
						
							    <?php the_excerpt(); ?>
					
						    </section> <!-- end article section -->
						
						    <footer class="article-footer">
								<hr />
						    </footer> <!-- end article footer -->
					
					    </article> <!-- end article -->
					
					    <?php endwhile; ?>	
					
					        <?php if (function_exists('johnny5_page_navi')) { ?>
						        <?php johnny5_page_navi(); ?>
					        <?php } else { ?>
						        <nav class="wp-prev-next">
							        <ul class="clearfix">
								        <li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "johnny5theme")) ?></li>
								        <li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "johnny5theme")) ?></li>
							        </ul>
					    	    </nav>
					        <?php } ?>
					
					    <?php else : ?>
					
    					    <article id="post-not-found" class="hentry clearfix">
    						    <header class="article-header">
    							    <!--<h1 class="page-title"><?php _e("Post Not Found!", "johnny5theme"); ?></h1>-->
    					    	</header>
    						    <section class="entry-content">
    							    <p style="text-align: center;"><?php _e("No posts at this time.", "johnny5theme"); ?></p>
        						</section>
    	    					<footer class="article-footer">
    		    				   
    			    			</footer>
    				    	</article>
					
					    <?php endif; ?>
			
    				</div> <!-- end #main -->
                
                </div> <!-- end #inner-content -->
                
			</div> <!-- end #content -->

<?php get_footer(); ?>
