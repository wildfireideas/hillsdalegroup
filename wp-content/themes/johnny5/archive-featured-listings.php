<?php get_header(); ?>
			
			<div id="content">
				
				<div class="header-image">
					<?php $page_header = of_get_option('page_header'); ?>
					
					<?php if (!empty($page_header)){ ?>
					 	
					 	<img src="<?php echo home_url(); ?>/<?php echo $page_header; ?>">

					<?php } ?>
				</div>
				
				<div id="inner-content">
					
					<div class="intro-bar">
						<div class="row">
							<div class="large-12 columns">
							</div>
						</div>
					</div>
					
					<div class="row" id="main" role="main">
			
					    <div class="large-12 columns clearfix">
					    
						    <h1 class="page-title">Featured Listings</h1>
						    
						<ul class="large-block-grid-4 featured-properties"> 

						    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						    
						    <li>
						
							    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
									
									<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
									    <section class="feature-image">
										  	  <?php the_post_thumbnail( 'johnny5-feature' ); ?>
									    </section> <!-- end article section -->
									    
									    <header class="article-header">			
										    <h2><?php the_title(); ?></h2>
									    </header> <!-- end article header -->
									</a>
							    </article> <!-- end article -->
						    
						    </li>
						
						    <?php endwhile; ?>	
						</ul>
						 
					        <?php if (function_exists('johnny5_page_navi')) { ?>
						        <?php johnny5_page_navi(); ?>
					        <?php } else { ?>
						        <nav class="wp-prev-next">
							        <ul class="clearfix">
								        <li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "johnny5theme")) ?></li>
								        <li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "johnny5theme")) ?></li>
							        </ul>
					    	    </nav>
					        <?php } ?>
					
					    <?php else : ?>
					
    					    <article id="post-not-found" class="hentry clearfix">
    						    <header class="article-header">
    							    <!--<h1 class="page-title"><?php _e("Post Not Found!", "johnny5theme"); ?></h1>-->
    					    	</header>
    						    <section class="entry-content">
    							    <p style="text-align: center;"><?php _e("No posts at this time.", "johnny5theme"); ?></p>
        						</section>
    	    					<footer class="article-footer">
    		    				   
    			    			</footer>
    				    	</article>
					
					    <?php endif; ?>
			
    				</div> <!-- end #main -->
                
                </div> <!-- end #inner-content -->
                
			</div> <!-- end #content -->

<?php get_footer(); ?>
