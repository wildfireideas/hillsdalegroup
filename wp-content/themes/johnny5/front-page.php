<?php get_header(); ?>
			
			<div id="content">
				
				<?php get_template_part( 'inc/home-carousel'); ?>

				<div id="inner-content">
					
					<div class="intro-bar">
						<div class="row">
							<div class="large-12 columns">
								<?php
									$welcome = of_get_option('welcome_text');
								?>
								<?php if (empty($welcome)) { ?>
							
								<?php } elseif (!empty($welcome)) { ?>
									<p><?php echo $welcome; ?></p>
								<?php } ?>
							</div>
						</div>
					</div>
					
					<div class="row" id="main" role="main">
			
					    <div class="large-12 columns clearfix">
	
						    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
						    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
							
							    <section class="entry-content clearfix">
								   	<?php get_sidebar(); ?>
								    <?php the_content(); ?>
							    </section> <!-- end article section -->
	
							    <?php // comments_template(); // uncomment if you want to use them ?>
						
						    </article> <!-- end article -->
						
						    <?php endwhile; ?>		
						
						    <?php else : ?>
						    
						        <article id="post-not-found" class="hentry clearfix">
						            <header class="article-header">
						        	    <h1><?php _e("Oops, Post Not Found!", "johnny5theme"); ?></h1>
						        	</header>
						            <section class="entry-content">
						        	    <p><?php _e("Uh Oh. Something is missing. Try double checking things.", "johnny5theme"); ?></p>
						        	</section>
						        	<footer class="article-footer">
						        	    <p><?php _e("This is the error message in the index.php template.", "johnny5theme"); ?></p>
						        	</footer>
						        </article>
						
						    <?php endif; ?>
				
					    </div>
					    
					</div>
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
