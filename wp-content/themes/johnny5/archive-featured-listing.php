<?php get_header(); ?>
			
			<div id="content">
				
				<div class="header-image">
					<?php $page_header = of_get_option('page_header'); ?>
					
					<?php if (!empty($page_header)){ ?>
					 	
					 	<img src="<?php echo home_url(); ?>/<?php echo $page_header; ?>">

					<?php } ?>
				</div>
				
				<div id="inner-content">
					
					<div class="intro-bar">
						<div class="row">
							<div class="large-12 columns">
							</div>
						</div>
					</div>
					
					<div class="row" id="main" role="main">
			
					    <div class="large-12 columns clearfix">
					    
						    <h1 class="page-title">Featured Listings</h1>  

					    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
						
						    <header class="article-header">
							
							    <h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
							    <p class="byline"><?php
                    printf(__('Posted on <time class="updated" datetime="%1$s" pubdate>%2$s</time>.', 'johnny5theme'), get_the_time('Y-m-j'), get_the_time(get_option('date_format')), johnny5_get_the_author_posts_link());?></p>		
						
						    </header> <!-- end article header -->
					
						    <section class="entry-content clearfix">
						
							    <?php the_post_thumbnail( 'johnny5-thumb-300' ); ?>
						
							    <?php the_excerpt(); ?>
					
						    </section> <!-- end article section -->
						
						    <footer class="article-footer">
								<hr />
						    </footer> <!-- end article footer -->
					
					    </article> <!-- end article -->
					
					    <?php endwhile; ?>	
					
					        <?php if (function_exists('johnny5_page_navi')) { ?>
						        <?php johnny5_page_navi(); ?>
					        <?php } else { ?>
						        <nav class="wp-prev-next">
							        <ul class="clearfix">
								        <li class="prev-link"><?php next_posts_link(__('&laquo; Older Entries', "johnny5theme")) ?></li>
								        <li class="next-link"><?php previous_posts_link(__('Newer Entries &raquo;', "johnny5theme")) ?></li>
							        </ul>
					    	    </nav>
					        <?php } ?>
					
					    <?php else : ?>
					
    					    <article id="post-not-found" class="hentry clearfix">
    						    <header class="article-header">
    							    <!--<h1 class="page-title"><?php _e("Post Not Found!", "johnny5theme"); ?></h1>-->
    					    	</header>
    						    <section class="entry-content">
    							    <p style="text-align: center;"><?php _e("No posts at this time.", "johnny5theme"); ?></p>
        						</section>
    	    					<footer class="article-footer">
    		    				   
    			    			</footer>
    				    	</article>
					
					    <?php endif; ?>
			
    				</div> <!-- end #main -->
                
                </div> <!-- end #inner-content -->
                
			</div> <!-- end #content -->

<?php get_footer(); ?>
