<?php
/*------------------------------*/
/* Theme Setup					*/
/*------------------------------*/

// ititial functions
add_action('after_setup_theme','johnny5_start', 16);

function johnny5_start() {

    // launching operation cleanup
    add_action('init', 'johnny5_head_cleanup');
    // remove WP version from RSS
    add_filter('the_generator', 'johnny5_rss_version');
    // remove pesky injected css for recent comments widget
    add_filter( 'wp_head', 'johnny5_remove_wp_widget_recent_comments_style', 1 );
    // clean up comment styles in the head
    add_action('wp_head', 'johnny5_remove_recent_comments_style', 1);
    // clean up gallery output in wp
    add_filter('gallery_style', 'johnny5_gallery_style');

    // enqueue base scripts and styles
    add_action('wp_enqueue_scripts', 'johnny5_scripts_and_styles', 998);
    // ie conditional wrapper

    // launching this stuff after theme setup
    johnny5_theme_support();

    // adding sidebars to Wordpress (these are created in functions.php)
    add_action( 'widgets_init', 'johnny5_register_sidebars' );

    // cleaning up random code around images
    add_filter('the_content', 'johnny5_filter_ptags_on_images');
    // cleaning up excerpt
    add_filter('excerpt_more', 'johnny5_excerpt_more');
}

/*------------------------------*/
/* SVG Support   				*/
/*------------------------------*/
function cc_mime_types( $mimes ){
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

/*------------------------------*/
/* Theme Support Email          */
/*------------------------------*/

function dashboard_support_link() {
    echo "<p id='emailSupport'>For support email <a href=\"mailto:websupport@wildfireideas.com\">websupport@wildfireideas.com</a></p>";
}

// Now we set that function up to execute when the admin_notices action is called
add_action( 'admin_notices', 'dashboard_support_link' );

// We need some CSS to position the paragraph
function dashboard_support_css() {
    // This makes sure that the positioning is also good for right-to-left languages
    $x = is_rtl() ? 'left' : 'right';

    echo "
    <style type='text/css'>
    #emailSupport {
        float: $x;
        padding-$x: 15px;
        padding-top: 5px;       
        margin: 0;
        font-size: 11px;
    }
    </style>
    ";
}

add_action( 'admin_head', 'dashboard_support_css' );

/*------------------------------*/
/* Dashboard Clean up   */
/*------------------------------*/

function disable_default_dashboard_widgets() {
    global $wp_meta_boxes;
    // wp..
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
    // bbpress
    //unset($wp_meta_boxes['dashboard']['normal']['core']['bbp-dashboard-right-now']);
    // yoast seo
    //unset($wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget']);
    // gravity forms
    //unset($wp_meta_boxes['dashboard']['normal']['core']['rg_forms_dashboard']);
}
add_action('wp_dashboard_setup', 'disable_default_dashboard_widgets', 999);

/*------------------------------*/
/* Dashboard Wildfire Info  	*/
/*------------------------------*/

function custom_dashboard_widget() {
	echo '<a href="http://wildfireideas.com" target="_blank"><img src="';
	echo get_template_directory_uri();
	echo '/library/images/wildfire.png"></a>';
	echo "<p>If you need support please email <a href=\"mailto:websupport@wildfireideas.com\">websupport@wildfireideas.com</a>.</p>";
	echo "<p>(336)777-3473</p>";
}
function add_custom_dashboard_widget() {
	wp_add_dashboard_widget('custom_dashboard_widget', 'Wildfire Information', 'custom_dashboard_widget');
}
add_action('wp_dashboard_setup', 'add_custom_dashboard_widget');


/*------------------------------*/
// Orbit Gallery
/*------------------------------*/
add_filter('post_gallery', 'my_post_gallery', 10, 2);
function my_post_gallery($output, $attr) {
    global $post;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => ''
    ), $attr));

    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }

    if (empty($attachments)) return '';

    // Here's your actual output, you may customize it to your need
    $output = "<div class=\"slideshow-wrapper\">\n";
    $output .= "<div class=\"preloader\"></div>\n";
    $output .= "<ul data-orbit>\n";

    // Now you loop through each attachment
    foreach ($attachments as $id => $attachment) {
        // Fetch the thumbnail (or full image, it's up to you)
//      $img = wp_get_attachment_image_src($id, 'medium');
//      $img = wp_get_attachment_image_src($id, 'my-custom-image-size');
        $img = wp_get_attachment_image_src($id, 'full');
        $caption = $attachment->post_excerpt;
		
        $output .= "<li>\n";
        $output .= "<img src=\"{$img[0]}\" width=\"{$img[1]}\" height=\"{$img[2]}\" alt=\"\" />\n";
        if (!empty($caption)) {
        $output .= "<div class=\"orbit-caption\">$caption</div>\n";	        
        }
        $output .= "</li>\n";
    }

    $output .= "</ul>\n";
    $output .= "</div>\n";

    return $output;
}
/*------------------------------*/
	
	
/*------------------------------*/
/* <heead>  Cleanup				*/
/*------------------------------*/

function johnny5_head_cleanup() {
	// category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// index link
	remove_action( 'wp_head', 'index_rel_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
  // remove WP version from css
  add_filter( 'style_loader_src', 'johnny5_remove_wp_ver_css_js', 9999 );
  // remove Wp version from scripts
  add_filter( 'script_loader_src', 'johnny5_remove_wp_ver_css_js', 9999 );

}
// remove WP version from RSS
function johnny5_rss_version() { return ''; }

// remove WP version from scripts
function johnny5_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}

// remove injected CSS for recent comments widget
function johnny5_remove_wp_widget_recent_comments_style() {
   if ( has_filter('wp_head', 'wp_widget_recent_comments_style') ) {
      remove_filter('wp_head', 'wp_widget_recent_comments_style' );
   }
}

// remove injected CSS from recent comments widget
function johnny5_remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
  }
}

// remove injected CSS from gallery
function johnny5_gallery_style($css) {
  return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
}

/*------------------------------*/
/* Header Image                 */
/*------------------------------*/
/*$defaults = array(
    'header-text'            => false,
    'uploads'                => true,
);
add_theme_support( 'custom-header', $defaults );
*/
/*------------------------------*/
/* Script & Enqueing			*/
/*------------------------------*/

// loading modernizr and jquery, and reply script
function johnny5_scripts_and_styles() {
  global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
  if (!is_admin()) {

    // modernizr (without media query polyfill)
    wp_register_script( 'johnny5-modernizr', 'https://hillsdaleregroup.com/wp-content/themes/johnny5/foundation/bower_components/modernizr/modernizr.js', array(), '2.7.1', false );
    
    // adding Foundation scripts file in the footer
    wp_register_script( 'foundation-js', 'https://hillsdaleregroup.com/wp-content/themes/johnny5/foundation/bower_components/foundation/js/foundation.min.js', array( 'jquery' ), '', true );
   
    // register main stylesheet
    wp_register_style( 'johnny5-stylesheet', 'https://hillsdaleregroup.com/wp-content/themes/johnny5/foundation/stylesheets/app.css', array(), '', 'all' );
	
	// register icon font stylesheet
    wp_register_style( 'johnny5-foundation-icons', 'https://hillsdaleregroup.com/wp-content/themes/johnny5/library/foundation-icons/foundation-icons.css', array(), '', 'all' );

    // register slick carousel stylesheet
    wp_register_style( 'johnny5-slick-carousel', 'https://hillsdaleregroup.com/wp-content/themes/johnny5/library/js/slick-carousel/slick/slick.css', array(), '', 'all' );
    
    // register project stylesheet
    wp_register_style( 'johnny5-project-styles', 'https://hillsdaleregroup.com/wp-content/themes/johnny5/css/project.css', array(), '', 'all' );
    
	
    // ie-only style sheet
    //wp_register_style( 'johnny5-ie-only', get_stylesheet_directory_uri() . '/library/css/ie.css', array(), '' );

    // comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }

    //adding scripts file in the footer
    wp_register_script( 'johnny5-js', 'https://hillsdaleregroup.com/wp-content/themes/johnny5/library/js/scripts.js', array( 'jquery' ), '', true );
    wp_register_script( 'slick-js', 'https://hillsdaleregroup.com/wp-content/themes/johnny5/library/js/slick-carousel/slick/slick.min.js', array( 'jquery' ), '', true );

    // enqueue styles and scripts
    wp_enqueue_script( 'johnny5-modernizr' );
    wp_enqueue_script ('foundation-js');
    wp_enqueue_style( 'johnny5-stylesheet' );
    wp_enqueue_style( 'johnny5-foundation-icons' );
    wp_enqueue_style( 'johnny5-slick-carousel' );
    wp_enqueue_style( 'johnny5-project-styles' );
    //wp_enqueue_style('johnny5-ie-only');

    //$wp_styles->add_data( 'johnny5-ie-only', 'conditional', 'lt IE 9' ); // add conditional wrapper around ie stylesheet

    /*
    I recommend using a plugin to call jQuery
    using the google cdn. That way it stays cached
    and your site will load faster.
    */
    wp_enqueue_script( 'johnny5-js' );
    wp_enqueue_script( 'slick-js' );
  }
}

//Replace jQuery with Google CDN jQuery
if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script( 'jquery', get_stylesheet_directory_uri() . '/foundation/bower_components/jquery/dist/jquery.min.js', false, null );
   wp_enqueue_script('jquery');
}

/*------------------------------*/
/* Theme Support/Options		*/
/*------------------------------*/

// Adding WP 3+ Functions & Theme Support
function johnny5_theme_support() {

	// wp thumbnails (sizes handled in functions.php)
	add_theme_support('post-thumbnails');

	// default thumb size
	set_post_thumbnail_size(125, 125, true);

	// wp custom background (thx to @bransonwerner for update)
	/*add_theme_support( 'custom-background',
	    array(
	    'default-image' => '',  // background image default
	    'default-color' => '', // background color default (dont add the #)
	    'wp-head-callback' => '_custom_background_cb',
	    'admin-head-callback' => '',
	    'admin-preview-callback' => ''
	    )
	);*/

	// rss 
	add_theme_support('automatic-feed-links');

	// to add header image support go here: http://themble.com/support/adding-header-background-image-support/

	// adding post format support, un-comment to utilize
	
	//add_theme_support( 'post-formats',
	//	array(
	//		'aside',             // title less blurb
	//		'gallery',           // gallery of images
	//		'link',              // quick link to other site
	//		'image',             // an image
	//		'quote',             // a quick quote
	//		'status',            // a Facebook like status update
	//		'video',             // video
	//		'audio',             // audio
	//		'chat'               // chat transcript
	//	)
	//);
	
	// wp menus
	add_theme_support( 'menus' );

}


/*------------------------------*/
/* Menus						*/
/*------------------------------*/

//a main menu and a footer menu are registered by default.

// the main menu
function johnny5_main_nav() {
	// display the wp3 menu if available
    wp_nav_menu(array(
    	'container' => false,                           // remove nav container
    	'container_class' => '',           // class of container (should you choose to use it)
    	'menu' => __( 'The Main Menu', 'johnny5theme' ),  // nav name
    	'menu_class' => 'left',         // adding custom nav class
    	'theme_location' => 'main-nav',                 // where it's located in the theme
    	'before' => '',                                 // before the menu
        'after' => '',                                  // after the menu
        'link_before' => '',                            // before each link
        'link_after' => '',                             // after each link
    	'fallback_cb' => 'johnny5_main_nav_fallback'      // fallback function
	));
}

// the footer menu
function johnny5_footer_links() {
	// display the wp3 menu if available
    wp_nav_menu(array(
    	'container' => '',                              // remove nav container
    	'container_class' => 'footer-links clearfix',   // class of container (should you choose to use it)
    	'menu' => __( 'Footer Links', 'johnny5theme' ),   // nav name
    	'menu_class' => 'nav footer-nav clearfix',      // adding custom nav class
    	'theme_location' => 'footer-links',             // where it's located in the theme
    	'before' => '',                                 // before the menu
        'after' => '',                                  // after the menu
        'link_before' => '',                            // before each link
        'link_after' => '',                             // after each link
        'depth' => 0,                                   // limit the depth of the nav
    	'fallback_cb' => 'johnny5_footer_links_fallback'  // fallback function
	));
}

// this is the fallback for header menu
function johnny5_main_nav_fallback() {
	wp_nav_menu(array(
    	'container' => false,                           // remove nav container
    	'container_class' => '',           // class of container (should you choose to use it)
       	'menu_class' => 'left',         // adding custom nav class
   	) );
}

// this is the fallback for footer menu
function johnny5_footer_links_fallback() {
	/* you can put a default here if you like */
}

// registering wp3+ menus
register_nav_menus(
	array(
		'main-nav' => __( 'The Main Menu' ),   // main nav in header
		'footer-links' => __( 'Footer Links' ) // secondary nav in footer
	)
);

// CONSIDER REPLACING THE FOLLOWING WITH A GOOD WALKER FUNCTION //

// Add "has-dropdown" CSS class to navigation menu items that have children in a submenu.
function nav_menu_item_parent_classing( $classes, $item )
{
    global $wpdb;
    
$has_children = $wpdb -> get_var( "SELECT COUNT(meta_id) FROM {$wpdb->prefix}postmeta WHERE meta_key='_menu_item_menu_item_parent' AND meta_value='" . $item->ID . "'" );
    
    if ( $has_children > 0 )
    {
        array_push( $classes, "has-dropdown" );
    }
    
    return $classes;
}
 
add_filter( "nav_menu_css_class", "nav_menu_item_parent_classing", 10, 2 );

	

//Deletes empty classes and changes the sub menu class name
function change_submenu_class($menu) {
    $menu = preg_replace('/ class="sub-menu"/',' class="dropdown"',$menu);
    return $menu;
}
add_filter ('wp_nav_menu','change_submenu_class');


/*------------------------------*/
/* Related Post					*/
/*------------------------------*/

// Related Posts Function (call using johnny5_related_posts(); )
function johnny5_related_posts() {
	echo '<ul id="johnny5-related-posts">';
	global $post;
	$tags = wp_get_post_tags($post->ID);
	if($tags) {
		foreach($tags as $tag) { $tag_arr .= $tag->slug . ','; }
        $args = array(
        	'tag' => $tag_arr,
        	'numberposts' => 5, /* you can change this to show more */
        	'post__not_in' => array($post->ID)
     	);
        $related_posts = get_posts($args);
        if($related_posts) {
        	foreach ($related_posts as $post) : setup_postdata($post); ?>
	           	<li class="related_post"><a class="entry-unrelated" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
	        <?php endforeach; }
	    else { ?>
            <?php echo '<li class="no_related_post">' . __( 'No Related Posts Yet!', 'johnny5theme' ) . '</li>'; ?>
		<?php }
	}
	wp_reset_query();
	echo '</ul>';
}

/*------------------------------*/
/* Page Navigation				*/
/*------------------------------*/

// Numeric Page Navi (built into the theme by default)
function johnny5_page_navi($before = '', $after = '') {
	global $wpdb, $wp_query;
	$request = $wp_query->request;
	$posts_per_page = intval(get_query_var('posts_per_page'));
	$paged = intval(get_query_var('paged'));
	$numposts = $wp_query->found_posts;
	$max_page = $wp_query->max_num_pages;
	if ( $numposts <= $posts_per_page ) { return; }
	if(empty($paged) || $paged == 0) {
		$paged = 1;
	}
	$pages_to_show = 7;
	$pages_to_show_minus_1 = $pages_to_show-1;
	$half_page_start = floor($pages_to_show_minus_1/2);
	$half_page_end = ceil($pages_to_show_minus_1/2);
	$start_page = $paged - $half_page_start;
	if($start_page <= 0) {
		$start_page = 1;
	}
	$end_page = $paged + $half_page_end;
	if(($end_page - $start_page) != $pages_to_show_minus_1) {
		$end_page = $start_page + $pages_to_show_minus_1;
	}
	if($end_page > $max_page) {
		$start_page = $max_page - $pages_to_show_minus_1;
		$end_page = $max_page;
	}
	if($start_page <= 0) {
		$start_page = 1;
	}
	echo $before.'<nav class="page-navigation"><ul class="pagination">'."";
	if ($start_page >= 2 && $pages_to_show < $max_page) {
		$first_page_text = __( "First", 'johnny5theme' );
		echo '<li class="bpn-first-page-link"><a href="'.get_pagenum_link().'" title="'.$first_page_text.'">'.$first_page_text.'</a></li>';
	}
	echo '<li class="bpn-prev-link">';
	previous_posts_link('<<');
	echo '</li>';
	for($i = $start_page; $i  <= $end_page; $i++) {
		if($i == $paged) {
			echo '<li class="current"><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
		} else {
			echo '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
		}
	}
	echo '<li class="bpn-next-link">';
	next_posts_link('>>');
	echo '</li>';
	if ($end_page < $max_page) {
		$last_page_text = __( "Last", 'johnny5theme' );
		echo '<li class="bpn-last-page-link"><a href="'.get_pagenum_link($max_page).'" title="'.$last_page_text.'">'.$last_page_text.'</a></li>';
	}
	echo '</ul></nav>'.$after."";
}

/*------------------------------*/
/* Misc.  Cleanup				*/
/*------------------------------*/

// remove the p from around imgs
function johnny5_filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

// This removes the annoying [�] to a Read More link
function johnny5_excerpt_more($more) {
	global $post;
	// edit here if you like
return '...  <a class="excerpt-read-more" href="'. get_permalink($post->ID) . '" title="'. __('Read', 'johnny5theme') . get_the_title($post->ID).'">'. __('Read more &raquo;', 'johnny5theme') .'</a>';
}

/*
 * This is a modified the_author_posts_link() which just returns the link.
 *
 * This is necessary to allow usage of the usual l10n process with printf().
 */
function johnny5_get_the_author_posts_link() {
	global $authordata;
	if ( !is_object( $authordata ) )
		return false;
	$link = sprintf(
		'<a href="%1$s" title="%2$s" rel="author">%3$s</a>',
		get_author_posts_url( $authordata->ID, $authordata->user_nicename ),
		esc_attr( sprintf( __( 'Posts by %s' ), get_the_author() ) ), // No further l10n needed, core will take care of this one
		get_the_author()
	);
	return $link;
}

/*------------------------------*/
/* Milti-Lingual				*/
/*------------------------------*/

//	Ads support for other languages
// require_once('library/translation/translation.php'); // disabled by default

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'johnny5-thumb-600', 600, 150, true );
add_image_size( 'johnny5-thumb-300', 300, 100, true );
add_image_size( 'johnny5-header', 1700, 503, true );
add_image_size( 'johnny5-slide', 1700, 700, true );
add_image_size( 'johnny5-feature', 270, 270, true );
add_image_size( 'agent', 250, 350, true );
/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'johnny5-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'johnny5-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function johnny5_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __('Sidebar 1', 'johnny5theme'),
		'description' => __('The first (primary) sidebar.', 'johnny5theme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __('Sidebar 2', 'johnny5theme'),
		'description' => __('The second (secondary) sidebar.', 'johnny5theme'),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
}

/*------------------------------*/
/* Comments Layout				*/
/*------------------------------*/

// Comment Layout
function johnny5_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class('panel'); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix large-12 columns">
			<header class="comment-author">
				<?php
				/*
					this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
					echo get_avatar($comment,$size='32',$default='<path_to_url>' );
				*/
				?>
				<!-- custom gravatar call -->
				<?php
					// create variable
					$bgauthemail = get_comment_author_email();
				?>
				<?php printf(__('<cite class="fn">%s</cite>', 'johnny5theme'), get_comment_author_link()) ?> on
				<time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__(' F jS, Y - g:ia', 'johnny5theme')); ?> </a></time>
				<?php edit_comment_link(__('(Edit)', 'johnny5theme'),'  ','') ?>
			</header>
			<?php if ($comment->comment_approved == '0') : ?>
				<div class="alert alert-info">
					<p><?php _e('Your comment is awaiting moderation.', 'johnny5theme') ?></p>
				</div>
			<?php endif; ?>
			<section class="comment_content clearfix">
				<?php comment_text() ?>
			</section>
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</article>
	<!-- </li> is added by WordPress automatically -->
<?php
} 
?>