<?php
	$facebook = of_get_option('facebook_url');
	$twitter = of_get_option('twitter_url');
	$youtube = of_get_option('youtube_url');
	$linkedin = of_get_option('linkedin_url');
?>
<nav role="navigation">
	<ul class="social-links">
		<?php if (empty($facebook)) { ?>
	
		<?php } elseif (!empty($facebook)) { ?>
			<li class="facebook"><a href="<?php echo $facebook; ?>" target="_blank">Facebook</a></li>
		<?php } ?>
		
		<?php if (empty($twitter)) { ?>
	
		<?php } elseif (!empty($twitter)) { ?>
			<li class="twitter"><a href="<?php echo $twitter; ?>" target="_blank">Twitter</a></li>
		<?php } ?>
		
		<?php if (empty($youtube)) { ?>
	
		<?php } elseif (!empty($youtube)) { ?>
			<li class="youtube"><a href="<?php echo $youtube; ?>" target="_blank">YouTube</a></li>
		<?php } ?>
		
		<?php if (empty($linkedin)) { ?>
	
		<?php } elseif (!empty($linkedin)) { ?>
			<li class="linkedIn"><a href="<?php echo $linkedin; ?>" target="_blank">LinkedIn</a></li>
		<?php } ?>
			<li class="blog"><a href="<?php echo home_url(); ?>/category/real-estate-blog">Blog</a></li>			   
	</ul>
</nav>