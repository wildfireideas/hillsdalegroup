<?php get_header(); ?>
			
			<div id="content">
				
				<div id="inner-content">
					
					<div class="intro-bar">
						<div class="row">
							<div class="large-12 columns">
							</div>
						</div>
					</div>
					
					<div class="row" id="main" role="main">
			
					    <div class="large-12 columns clearfix">
						    

						<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						
								<header class="article-header">
							
									<h1 class="page-title" itemprop="headline"><?php the_title(); ?></h1>
								
								</header> <!-- end article header -->
					
								<section class="entry-content clearfix" itemprop="articleBody" style="padding-top: 1rem;">
									
									<?php if(!((get_post_meta($post->ID, 'wpcf-agent-headshot', TRUE))=='')){ ?>
									<div class="row">
										<div class="large-3 columns" style="padding-top: 0.25rem;">
											<?php echo types_render_field("agent-headshot", array("size"=>"agent","alt"=>"%%ALT%%")); ?>
											<p><?php echo types_render_field("email"); ?><br/>
											   <?php echo types_render_field("phone-number"); ?>
											</p>
										</div>
										<div class="large-9 columns">
											<?php the_content(); ?>
										</div>
									</div>
									<?php } else { ?>
										<?php the_content(); ?>
										<p><?php echo types_render_field("email"); ?></p>
									<?php } ?>
								</section> <!-- end article section -->
						
								<footer class="article-footer">
									<?php the_tags('<p class="tags"><span class="tags-title">' . __('Tags:', 'johnny5theme') . '</span> ', ', ', '</p>'); ?>
									
									<p><a href="/about-us" class="back button">&laquo; Back To About</a></p>
								</footer> <!-- end article footer -->
					
								<?php //comments_template(); ?>
					
							</article> <!-- end article -->
					
						<?php endwhile; ?>		
					
						<?php else : ?>
					
							<article id="post-not-found" class="hentry clearfix">
					    		<header class="article-header">
					    			<h1><?php _e("Oops, Post Not Found!", "johnny5theme"); ?></h1>
					    		</header>
					    		<section class="entry-content">
					    			<p><?php _e("Uh Oh. Something is missing. Try double checking things.", "johnny5theme"); ?></p>
					    		</section>
					    		<footer class="article-footer">
					    		    <p><?php _e("This is the error message in the single.php template.", "johnny5theme"); ?></p>
					    		</footer>
							</article>
					
						<?php endif; ?>
						
			
					</div> <!-- end #main -->

				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
