			<footer class="footer" role="contentinfo">

				<div class="upper-footer">
				</div>

				<div class="lower-footer">

					<div class="row clearfix">

						<div class="large-8 medium-8 columns">

							<h4>Contact</h4>
							<?php get_template_part( 'inc/social-media'); ?>

							<?php
								$phone = of_get_option('phone_number');
								$address = of_get_option('address');
								$email = of_get_option('email');
							?>
							<ul class="contact-info">
								<?php if (empty($phone)) { ?>

								<?php } elseif (!empty($phone)) { ?>
									<li class="phone"><?php echo $phone; ?></li>
								<?php } ?>

								<?php if (empty($address)) { ?>

								<?php } elseif (!empty($address)) { ?>
									<li class="address"><?php echo $address; ?></li>
								<?php } ?>

								<?php if (empty($email)) { ?>

								<?php } elseif (!empty($email)) { ?>
									<li class="email"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></li>
								<?php } ?>
							</ul>
						</div>
						<div class="large-4 medium-4 columns">
							<a href="http://bit.ly/2AuCCCP" target="_blank"><img border="0" width="140" height="140" src="https://hillsdaleregroup.com/wp-content/uploads/2019/01/rate36_140x140.png" alt="rate us button"/></a>
	    				</div>
						<div class="large-12 medium-12 columns">
							<nav role="navigation">
	    						<?php johnny5_footer_links(); ?>
	    					</nav>
	    					<div class="wildfire">
	    						<a href="" target="_blank">
		    						<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
									 viewBox="0 0 326.5 81.4" enable-background="new 0 0 326.5 81.4" xml:space="preserve" width="100px" height="29px">
									<g>
										<path fill="none" d="M145.3,54c0,12.1,6.1,17.4,12.6,17.4c3.9,0,8-1.4,11.9-5.3V38.4c-3.4-1.1-6.5-1.5-8.9-1.5
											C151.3,36.9,145.3,43,145.3,54z"/>
										<path fill="none" d="M305,35.9c-5.7,0-11.1,4-11.5,12.1l21.9-0.1C315.7,40.4,311.4,35.9,305,35.9z"/>
										<polygon fill="#FFFFFF" points="113.5,80.3 125.7,80.3 125.7,0.6 113.5,2.2 	"/>
										<path fill="#FFFFFF" d="M169.8,28.6c-2.8-0.7-5.8-1-8.8-1c-17.7,0-28.1,11.3-28.1,28c0,16.9,9.6,25.8,21.6,25.8
											c5.6,0,11.2-1.8,15.9-7.1l0.5,6l11,0V0.6l-12.1,1.6V28.6z M169.8,66.1c-3.9,3.9-8,5.3-11.9,5.3c-6.5,0-12.6-5.3-12.6-17.4
											c0-11,6-17.1,15.6-17.1c2.4,0,5.5,0.4,8.9,1.5V66.1z"/>
										<polygon class="orange" fill="#F7941E" points="226.9,80.3 239,80.3 239,27.9 226.9,29.6 	"/>
										<path class="orange" fill="#F7941E" d="M222.4,0.2c-10.4,0-17,2.8-21.2,8.1c-4.2,5.1-5,12.1-5,19.5v1.9l-8.2,0.9v8.3h8.2v41.3h12.1V38.9h12.9
											l0.5-9.2h-13.4v-2.6c0-5.6,0.2-10.3,2.9-13.3c2.3-2.7,5.8-4,11.3-4c2.8,0,6.3,0.3,10.6,1l1.3-9.3C230.2,0.5,226.2,0.2,222.4,0.2z"
											/>
										<path class="orange" fill="#F7941E" d="M275.2,27.6c-4.7,0-9.7,1-13.6,8.4l-0.2-8l-11.1,1.5v50.7h12.2V44.4c3.4-4.7,7.8-6.2,11.7-6.2
											c1.8,0,3.4,0.2,5.3,0.6l1.4-10.5C279,27.8,276.4,27.6,275.2,27.6z"/>
										<path class="orange" fill="#F7941E" d="M310.3,71.8c-11,0-17.8-5.6-17.9-16.1h33.3c2.4-17.9-6.2-28.2-20.2-28.2c-15.2,0-25.1,10.7-25.1,26.5
											c0,15.7,8.8,27.5,28.2,27.4c6,0,11.1-0.9,16.5-3.5l1.2-9.3C320.2,70.9,314.8,71.8,310.3,71.8z M304,35.9c6.4,0,10.6,4.5,10.4,12
											L292.6,48C293,39.9,298.3,35.9,304,35.9z"/>
										<path fill="#FFFFFF" d="M63.1,60.5c-1.3,4.5-2.3,7.9-2.3,8.1l-2.2-8.3l-9.7-31.8H36.5L26.2,60.6c-1.4,4-2.4,7.6-2.5,8.1
											c-0.1-0.4-0.9-4.1-2-8.1l-9.1-32.1H0l15.8,51.8H30l12.2-36.9l11.4,36.9h14l16.6-50.7l-11.5-1.1L63.1,60.5z"/>
										<polygon fill="#FFFFFF" points="90,80.3 102.2,80.3 102.2,31.3 90,30.2 	"/>
										<polygon class="orange" fill="#F7941E" points="90.6,0 89,17 60.8,21.7 102.3,25.5 107.6,10.1 98.9,18 	"/>
									</g>
									</svg>

	    							<p>Designed &amp; Developed</p>
	    						</a>
	    					</div>
	    				</div>

					</div>

				</div>

			</footer> <!-- end footer -->

		</div> <!-- end #container -->

		<!-- all js scripts are loaded in library/johnny5.php -->
		<?php wp_footer(); ?>

<script>
	$(document).ready(function(){
		$('.slick-carousel').slick({
		  dots: true,
		  arrows: false,
		  autoplay: true,
  		  autoplaySpeed: 3000,
  		  fade: true,
  		  cssEase: 'linear'
		});
	});
</script>

<script>
	(function($) {
		$(document).foundation();
	})(jQuery);
</script>
<!--<script src="//www.siteminds.net/m/1.7/mind_loader.php?pid=x4w9B5z5p31&cast_id=v1532315&autoplay=1&avname=jackie&wc=1&avnum=15&band_type=av" async></script>-->
<script src="//www.elocallink.tv/m/1.7/band_loader.php?pid=x6wBz4p3&cast_id=y2NwzB44&autoplay=0&framenum=19&g_o=bfl&wc=1&ss=1&mid=161543&band_type=ecastp" async></script>
<script type="application/ld+json">
{
"@context": "https://schema.org",
"@type": "RealEstateAgent",
"name": "Hillsdale Real Estate Group",
"image": "https://hillsdaleregroup.com/wp-content/themes/johnny5/library/images/hillsdale-logo.svg",
"@id": "",
"url": "https://hillsdaleregroup.com/",
"telephone": "https://hillsdaleregroup.com/",
"address": {
"@type": "PostalAddress",
"streetAddress": "5539 US-158",
"addressLocality": "Advance",
"addressRegion": "NC",
"postalCode": "27006",
"addressCountry": "US"
},
"geo": {
"@type": "GeoCoordinates",
"latitude": 36.007623,
"longitude": -80.4300877
},
"openingHoursSpecification": {
"@type": "OpeningHoursSpecification",
"dayOfWeek": [
"Monday",
"Tuesday",
"Wednesday",
"Thursday",
"Friday"
],
"opens": "09:00",
"closes": "17:00"
}
}
</script>
</body>

</html> <!-- end page -->
