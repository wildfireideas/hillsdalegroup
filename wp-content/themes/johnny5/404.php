<?php get_header(); ?>
			
			<div id="content">
				
				<div class="header-image">
					<?php $page_header = of_get_option('page_header'); ?>
					
					<?php if (!empty($page_header)){ ?>
					 	
					 	<img src="<?php echo home_url(); ?>/<?php echo $page_header; ?>">

					<?php } ?>
				</div>
				
				<div id="inner-content">
					
					<div class="intro-bar">
						<div class="row">
							<div class="large-12 columns">
							</div>
						</div>
					</div>
					
					<div class="row" id="main" role="main">
			
					    <div class="large-12 columns clearfix">
						    
						<article id="post-not-found" class="hentry clearfix">
						
							<header class="article-header">
							
								<h1><?php _e("Error 404 - Page Not Found", "johnny5theme"); ?></h1>
						
							</header> <!-- end article header -->
					
							<section class="entry-content">
							
								<p><?php _e("The page you were looking for was not found, but maybe try looking again!", "johnny5theme"); ?></p>
					
							</section> <!-- end article section -->

							<section class="search">
				
							    <p><?php get_search_form(); ?></p>
				
							</section> <!-- end search section -->
						
							<footer class="article-footer">
							
							</footer> <!-- end article footer -->
					
						</article> <!-- end article -->
			
					</div> <!-- end #main -->

				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>
