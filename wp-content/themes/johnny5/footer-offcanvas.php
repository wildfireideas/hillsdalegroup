			<footer class="footer" role="contentinfo">
			
				<div id="inner-footer" class="row clearfix">
				
					<div class="large-12 medium-12 columns">
						<nav role="navigation">
    						<?php johnny5_footer_links(); ?>
    					</nav>
    				</div>
	               
	                <div class="large-12 medium-12 columns">		
						<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?>.</p>
					</div>
				
				</div> <!-- end #inner-footer -->
				
			</footer> <!-- end footer -->
		
		</div> <!-- end #container -->
		</div>
		</div>
			
		<!-- all js scripts are loaded in library/johnny5.php -->
		<?php wp_footer(); ?>

<script>
	$(document).ready(function(){
		$('.slick-carousel').slick({
		  dots: true,
		  fade: true,
		  arrows: true,
		  autoplay: true,
  		  autoplaySpeed: 2000
		});
	});
</script>	

<script>
	(function($) {
		$(document).foundation();
	})(jQuery);
</script>


	</body>

</html> <!-- end page -->
