<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">
		<title><?php wp_title(''); ?></title>

		<!-- Google Chrome Frame for IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="p:domain_verify" content="e40249ae025627b03aed5c11ddde953c"/>
		
		<!-- mobile meta -->
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<!-- icons & favicons -->
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<!-- or, set /favicon.ico for IE10 win -->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
		
		<!--type kit-->
		<script src="//use.typekit.net/roc1rgy.js"></script>
		<script>try{Typekit.load();}catch(e){}</script>

  	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->

		<!-- drop Google Analytics Here -->
		<!-- end analytics -->

	</head>

	<body <?php body_class(); ?>>

			<div id="container">
	
				<header class="header" role="banner">
	
					<div id="inner-header" class="row">
						<div class="large-4 columns logo">
							<a href="<?php echo home_url(); ?>"><img src="https://hillsdaleregroup.com/wp-content/themes/johnny5/library/images/hillsdale-logo.svg" alt="Hillsdale Real Estate logo"></a>
						</div>
						
						<div class="large-8 columns">
							<div class="row">
								<div class="large-12 columns">
									<?php get_template_part( 'inc/social-media'); ?>
								</div>
							</div>
							<div class="row">
								<div class="large-12 columns">
									<nav class="top-bar" data-topbar role="navigation">
									  <ul class="title-area">
									    <li class="name">
									      <!--<h1> <a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a></h1>-->
									    </li>
									     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
									    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
									  </ul>
									
									  <section class="top-bar-section">
									    
									    <!-- Left Nav Section -->
									    <?php johnny5_main_nav(); ?>
								
									  </section>
									</nav>
								</div>
							</div>						
						</div>
					</div> <!-- end #inner-header -->
	
				</header> <!-- end header -->
			
